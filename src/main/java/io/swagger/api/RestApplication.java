package io.swagger.api;

import io.swagger.api.impl.LoginApiServiceImpl;
import io.swagger.api.impl.UsersApiServiceImpl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class RestApplication extends Application {


    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<Class<?>>();
        resources.add(LoginApiServiceImpl.class);
        resources.add(UsersApiServiceImpl.class);

        return resources;
    }


}