package io.swagger.api.impl;

import io.swagger.api.UsersApi;
import io.swagger.model.UserDTO;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyEapServerCodegen", date = "2017-03-22T09:52:50.787+01:00")
public class UsersApiServiceImpl implements UsersApi {
    public Response getUsers(SecurityContext securityContext) {
        List<UserDTO> users = new ArrayList<UserDTO>();
        for (int i = 0; i < 10; i++) {
            UserDTO user = new UserDTO();
            user.setId(i);
            user.setName("Teszt Elek " + i);
            users.add(user);
        }
        return Response.ok().entity(users).build();
    }
}
