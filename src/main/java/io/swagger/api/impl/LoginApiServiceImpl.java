package io.swagger.api.impl;

import io.swagger.api.LoginApi;
import io.swagger.model.LoginDTO;
import io.swagger.model.LoginResponseDTO;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyEapServerCodegen", date = "2017-03-22T09:52:50.787+01:00")
public class LoginApiServiceImpl implements LoginApi {
    public Response login(LoginDTO loginDTO, SecurityContext securityContext) {
        LoginResponseDTO res = new LoginResponseDTO();
        res.setName(loginDTO.getUsername());
        res.setToken("dsadasdafda");
        return Response.ok().entity(res).build();
    }
}
