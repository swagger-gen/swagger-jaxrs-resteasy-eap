# Swagger jaxrs-resteasy-eap generated server

```
java -jar swagger-codegen-cli-2.2.2.jar generate -i swagger.json -l jaxrs-resteasy-eap
```

Generálás utáni teendők:
* Impl osztályokban a logika megvalósítása

Előnyök:
* Kis mennyiségű generált kód. Csak Api (Interface) és Model (DTO) osztályok

Hátrányok:
* válasz vázát csak annotációban jelöli (amit nem kényszerít ki), mindenhol javax.ws.rs.core.Response tér vissza amibe a megfelelő osztályt kell beletenni
```
    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = UserDTO.class, responseContainer = "List", tags = {"User",})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "user lista", response = UserDTO.class, responseContainer = "List")})
    public Response getUsers(@Context SecurityContext securityContext);
```
* pom-ban sok függőség alapból (a nem használtak is)
